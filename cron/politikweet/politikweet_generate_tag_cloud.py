import mongoengine
import datetime
import time
import logging
import random
from mongoengine.queryset.visitor import Q as _Q
from collections import OrderedDict
from cron.politikweet.documents import KeywordAnalysisUnit, Sentimentalite

# mongo engine
MONGO_HOST = 'ds041861.mlab.com:41861'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine

_MONGODB_DATABASE_HOST__ = \
    'mongodb://%s:%s@%s/%s' \
    % (MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOST, MONGO_DATABASE)

mongoengine.connect(
     host="""mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority"""

)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger('politikweet-generate-tag-cloud')


def politikweet_generate_tag_cloud():
    """
    calculate keyword analysis unit for tag cloud
    """
    start_time = time.time()
    random_slice = random.choice([233, 377, 610])
    logger.warning("""**** GENERATE TAG CLOUD START {}""".format(start_time))
    lower_limit = datetime.datetime.utcnow() - datetime.timedelta(days=90)
    upper_limit = datetime.datetime.utcnow()
    logger.info("""       >>> SENTIMENTALITE RANDOM SLICE {} RANGE FROM:{}  TO:{}""".format(
        random_slice, lower_limit, upper_limit))
    keywords = Sentimentalite.objects.filter(
        (_Q(key_words__startswith="@") | _Q(key_words__startswith="#")) &
        (_Q(created_date__gte=lower_limit) & _Q(created_date__lte=upper_limit))
    ).order_by('created_date')[:random_slice]

    logger.info("""       >>> SENTIMENTALITES IN PLAY {}""".format(len(keywords)))

    keywords_frequency = keywords.item_frequencies(field='key_words', normalize=False, map_reduce=True)

    logger.info("""       >>> KEYWORDS IN PLAY {}""".format(len(keywords_frequency)))

    filtered_sorted_keywords = OrderedDict({
        key: value for (key, value) in sorted(keywords_frequency.items(), reverse=True, key=lambda item: item[1])
        if value > 3 and len(key) > 5 and (key.startswith('#') or key.startswith('@'))
    })

    logger.info("""           >>> FILTERED KEYWORDS IN PLAY {}""".format(len(filtered_sorted_keywords)))

    to_save = []
    for key, value in filtered_sorted_keywords.items():
        logger.debug("""           >>> KEYWORD {}""".format(key))
        payload = KeywordAnalysisUnit()
        payload['key'] = key
        payload['value'] = float(value)

        sentimentalites = Sentimentalite.objects.filter(
            _Q(key_words=key) & _Q(created_date__gte=lower_limit) & _Q(created_date__lte=upper_limit))

        # calculate average sentiment
        _keyword_avg_sentiment_ = sum([
            item.to_mongo().get('sentiment_score', {}).get('compound', {}) for item in sentimentalites
        ]) / len(sentimentalites)
        payload['sentiment'] = _keyword_avg_sentiment_

        to_save.append(payload)

    KeywordAnalysisUnit.drop_collection()
    KeywordAnalysisUnit.objects.insert(to_save)

    logger.info("""        >>> KeywordAnalysisUnit collection recreated {}""".format(len(to_save)))

    duration = time.time() - start_time

    logger.warning("""**** GENERATE TAG CLOUD END DURATION {}""".format(duration))


politikweet_generate_tag_cloud()
