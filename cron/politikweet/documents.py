# Create your models here.

from __future__ import absolute_import, unicode_literals

from datetime import datetime

from mongoengine import DynamicDocument, DateTimeField, StringField


class Base(DynamicDocument):
    """Base superclass for all models"""
    created_date = DateTimeField(default=datetime.now())
    meta = {'allow_inheritance': True}


class Sentimentalite(Base):
    """dynamic document describing an analysis unit"""
    member_of = StringField(max_length=50)


class KeywordAnalysisUnit(DynamicDocument):
    created_date = DateTimeField(default=datetime.now())

class NamedEntity(DynamicDocument):
    created_date = DateTimeField(default=datetime.now())

class Tweetstream(DynamicDocument):
    created_date = DateTimeField(default=datetime.utcnow())


class TwitterSentimentReduction(DynamicDocument):
    created_date = DateTimeField(default=datetime.utcnow())


class SentimentAccumulator(DynamicDocument):
    created_date = DateTimeField(default=datetime.utcnow())
