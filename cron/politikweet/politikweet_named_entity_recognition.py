import collections
import datetime
import logging
import random
import time

import mongoengine
import nltk
from mongoengine.queryset.visitor import Q as _Q

from cron.politikweet.documents import Sentimentalite, NamedEntity

# mongo engine
MONGO_HOST = 'ds041861.mlab.com:41861'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine

_MONGODB_DATABASE_HOST__ = \
    'mongodb://%s:%s@%s/%s' \
    % (MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOST, MONGO_DATABASE)

mongoengine.connect(
     host="""mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority"""

)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger('politikweet-generate-named-entity_recognition')

# */21 * * * * PYTHONPATH=/root/combine /root/combine/env/bin/python /root/combine/cron/politikweet/politikweet_named_entity_recognition.py  >> /root/combine/cron/politikweet/logs/politikweet-tag-cloud-generate.log 2>&1
def extract_entity_names(t):
    entity_names = []
    if hasattr(t, 'label') and t.label:
        if t.label() == 'NE':
            entity_names.append(' '.join([child[0] for child in t]))
        else:
            for child in t:
                entity_names.extend(extract_entity_names(child))
    return entity_names


def politikweet_generate_named_entity_recognition():
    """
    get the named entities
    """
    start_time = time.time()
    random_slice = random.choice([233, 377, 610])
    logger.warning("""**** START :: GENERATE NAMED ENTITIES  {}""".format(start_time))
    lower_limit = datetime.datetime.utcnow() - datetime.timedelta(days=90)
    upper_limit = datetime.datetime.utcnow()
    logger.info("""       >>> SENTIMENTALITE RANDOM SLICE {} RANGE FROM:{}  TO:{}""".format(
        random_slice, lower_limit, upper_limit))
    sentimentalites = Sentimentalite.objects.filter(
        (_Q(created_date__gte=lower_limit) & _Q(created_date__lte=upper_limit))
    ).order_by('created_date')[:random_slice]

    extractions = []
    for sentimentalite in sentimentalites:
        text = sentimentalite.to_mongo().get('tweet_payload', {}).get('raw', {})
        sentences = nltk.sent_tokenize(text)
        tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
        tagged_sentences = [nltk.pos_tag(sentence) for sentence in tokenized_sentences]
        chunked_sentences = nltk.ne_chunk_sents(tagged_sentences, binary=True)
        entities = []

        for tree in chunked_sentences:
            entities.extend(extract_entity_names(tree))
            logger.debug("""           >>> ENTITIES TREE LENGTH :: {}""".format(len(tree)))

        for entity in entities:
            if entity not in "RT":
                extractions.append(entity.title())

        to_save = []
        for item in collections.Counter(list(extractions)).most_common(10):
            payload = NamedEntity()
            payload['ne'] = {
                item[0]: item[1]
            }
            to_save.append(payload)

    NamedEntity.drop_collection()

    for i in to_save:
        try:
            i.save()
        except Exception as e:
            print(repr(e))
            continue

    duration = time.time() - start_time

    logger.warning("""**** END :: GENERATE NAMED ENTITIES DURATION {}""".format(duration))


politikweet_generate_named_entity_recognition()
