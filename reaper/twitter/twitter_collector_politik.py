# Create your views here.


import datetime
import random
import re
import time
import pprint
from urllib.parse import urlparse

import mongoengine
import nltk
import requests
import tweepy
from bs4 import BeautifulSoup
from newsapi import NewsApiClient
import reaper.sentimentacron as sentimentacron
from reaper.twitter.models import Sentimentalite, Tweetstream
from reaper.utils import urldecode
from newspaper import Article
from mongoengine.queryset.visitor import Q

# mongo engine
MONGO_HOST = 'ds041861.mongolab.com'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine
_MONGOURI = 'mongodb://{dbuser}:{dbpassword}@ds041861.mlab.com:{port}/criticalitude'.format(
    dbuser=MONGO_USERNAME,
    dbpassword=MONGO_PASSWORD,
    port=MONGO_PORT
)

mongoengine.connect(
   host="""mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority"""
)

TWITTER_CONSUMER_KEY = "xhtpkD4JV7qlyizDGe0tLg"
TWITTER_CONSUMER_SECRET = "STO0YHWjcFdHEQlPC8A8va1ZPjLUshZPFJxsnOasvA"
TWITTER_ACCESS_TOKEN = "39590750-2Z0C9AC3wozhcsjgACYUfVlTama85T2UbbD1vraOd"
TWITTER_ACCESS_TOKEN_SECRET = "p9IPiJuiEJhzgXP8XiuC6aKIdqPXxiHxREf1T1PJ6a4"

NEWS_ORG_API_KEY = '7deb02e1d96b4f35bc5574a0bfab4fab'

NEWS = NewsApiClient(api_key=NEWS_ORG_API_KEY)

auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
auth.set_access_token(TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET)

API = tweepy.API(auth_handler=auth)
SENTI = sentimentacron.Sentimentalyzer()


def tweetIsNotReply(tweet):
    tweet = tweet._json
    if (tweet['in_reply_to_status_id']
            or tweet['in_reply_to_status_id_str']
            or tweet['in_reply_to_user_id']
            or tweet['in_reply_to_user_id_str']
            or tweet['in_reply_to_screen_name']):
        print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: IS REPLY? TRUE'.format())
        return True
    else:
        print(':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: IS REPLY? FALSE'.format())
        return False

def isGoodURL(url,text):
    response = "twitter" not in url and "youtube" not in url and "t.co" not in url and not url.endswith('pdf') and  (
            "log out" not in text.lower() or
            "authenticate" not in text.lower() or
            "terms of service" not in text.lower() or
            "privacy policy" not in text.lower() or
            "login" not in text.lower()
    )
    print('::::::::::::: IS GOOD URL {}'.format(response))
    return response

def goForNewsApi():
    return False

class MyStreamListener(tweepy.StreamListener):
    def on_connect(self):
        print(":::::::::::::::::::::::::::: CONNECTED ::::::::::::::::::::::::::::::::")

    def on_error(self, status_code):

        if status_code == 420:
            # returning False in on_data disconnects the stream
            zzzz = random.randint(10, 16)
            time.sleep(zzzz)
            print("         ::::::::::::::::::::::::::::::::: ON_ERROR STAUS 420  {} s".format(zzzz))
            raise RuntimeError("420 ENHANCE YOUR CALM.")

    def on_exception(self, exception):
        """Called when an unhandled exception occurs."""
        zzzz = random.randint(3, 8)
        print("         ::::::::::::::::::::::::: ON_EXCEPTION {} : SLEEPING FOR  {} s".format(exception,zzzz))
        time.sleep(zzzz)
        raise exception

    def on_status(self, status):
        """Called when a new status arrives"""
        print("::: POLITIKWEET BEGIN ::: {} ::::::::::::::::::::".format(datetime.datetime.utcnow().isoformat()))

        key_words = []
        TOKEN_TYPES_TO_KEEP = ["NN", "NS", "NNP", "NNS", "JJ", "RB", "VBD", "NNPS", "VB"]

        expanded_urls = [
            i['expanded_url'] for i in status.entities.get('urls', False) if 'twitter' not in i['expanded_url']
        ]

        if not tweetIsNotReply(status):
            print(" ::::::::::::::::::::: ANALYZE ::: {} {}".format(datetime.datetime.utcnow().isoformat(), ":" * 30))
            tweet_text = status.text
            print(" ::::::::::::::::::::: TWEET TEXT {} ::".format(tweet_text))
            tweet_id = status.id

            urls = expanded_urls
            hash_tags = [i['text'] for i in status.entities.get('hashtags')]
            mentions = [i['screen_name'] for i in status.entities.get('user_mentions')]
            geo = status.geo

            tokens = nltk.word_tokenize(tweet_text)
            tokens = nltk.pos_tag(tokens)

            print("        ::TOKENS::{}".format(tokens))
            regex_hashtags = re.compile("#(?:\w+)")
            _hash = re.findall(regex_hashtags, tweet_text)
            regex_mentions = re.compile("@(?:\w+)")
            _ments = re.findall(regex_mentions, tweet_text)
            regex_url = re.compile("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")

            print("        :: HASHTAGS::{}".format(hash_tags))

            print("        :: MENTIONS::{}".format(mentions))

            print("        :: URLS    ::{}".format(repr(urls)))

            tweet_text_no_url = re.sub(r"http\S+", "", tweet_text)
            tweet_for_analysis = re.findall(r'\w+', tweet_text_no_url)
            tweet_for_analysis = " ".join(tweet_for_analysis)
            tweet_for_analysis = tweet_for_analysis + "."

            print("        ::::::::::::::::::::::: ANALYSIS TEXT {}".format(tweet_for_analysis))

            # well be looking for NN NNS NNP
            for tuple in tokens:
                if tuple[0] not in ["http", "https"] and len(tuple[0]) > 4 and (tuple[1] in TOKEN_TYPES_TO_KEEP):
                    key_words.append(tuple[0])

            proper_nouns = [token[0] for token in tokens if
                            token[1] in ['NN', 'NNP'] and token[0] not in ['http', 'https', '@'] and not token[
                                0].startswith('//')]

            print("     :::::::::::: PROPER NOUNS {}".format(proper_nouns))

            key_words.extend(_hash)
            key_words.extend(_ments)
            key_words = list(set([zxz for zxz in key_words if not zxz.startswith("//")]))

            # urls.extend(_urls)

            print("        ::::::::::::::::::::::: KEYWORDS : {}".format(repr(key_words)))

            sentiment_score = SENTI.sentiment_analysis(tweet_for_analysis)

            data = {
                'compound': sentiment_score['compound']
            }

            absolute_sentiment = SENTI.absolute_sentiment(data)

            print("         ::: SENTIMENT SCORE :::  {} {}".format(repr(sentiment_score), repr(absolute_sentiment)))

            tweet_payload = {
                "raw": tweet_text,
                "tokenized": tweet_for_analysis,

            }

            # here lets follow the links one level out
            embd_url_payload = {}
            each_sentence_score = []

            if not urls and proper_nouns and goForNewsApi():

                q = random.choice(proper_nouns)
                print("  ::::::::::::::::::::::::::::::::::::: GOING FOR NEWS API {} ".format(q, ))
                response = NEWS.get_top_headlines(
                    q=q,
                    page_size=10)

                random_article = random.choice(response['articles']) if response['articles'] else None
                _sleep = random.uniform(.1, .3) ** random.uniform(.5, .7)
                print("   :::::::::::::::::; MID SLEEP ::::: {} RANDOM ARTICLE --> {}".format(_sleep,
                                                                                              bool(random_article)))

                if random_article:
                    urls.append(random_article['url'])

                time.sleep(_sleep)
                print

            if urls:

                print(":::::: ATTEMPT FETCH URL ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
                try:

                    _url_ = random.choice(urls)
                    response = Article(_url_)
                    response.download()
                    response.parse()
                    response.nlp()
                    embd_url_payload['url'] = response.url
                    embd_url_payload['title'] = response.title
                    embd_url_payload['keywords'] = response.keywords
                except Exception as e:
                    print(repr(e))
                    response = None


                print(":::::::::::::::::::::::::::::::: REDIRECT URL {}".format(getattr(response, 'url', None)))

                found_it_lower_limit = datetime.datetime.utcnow() + datetime.timedelta(days=-30)
                found_it_upper_limit = datetime.datetime.utcnow()

                q_lookup = Q(embedded_url_payload__title__icontains=response.title if response else None)
                q_lookup |= Q(embedded_url_payload__url__icontains=response.url if response else None)
                found_it = Sentimentalite.objects.filter(
                    q_lookup,
                    created_date__gte=found_it_lower_limit.replace(hour=0,minute=0,second=0),
                    created_date__lte=found_it_upper_limit.replace(hour=0,minute=0,second=0)

                )

                if response and not found_it:
                    sentences = nltk.sent_tokenize(response.text)

                    print("             ::::::::::::: NUMBER of SENTENCES {} ::".format(len(sentences)))

                    # must have this many sentences to calculate a proper sample
                    if len(sentences) >= 3:
                        print("         ::: LONG ENOUGH AND WERE SAVING THIS :::::: {} ::::::::".format(not bool(found_it)))

                        for sentence in sentences:
                            sentence_sentiment_score = SENTI.sentiment_analysis(sentence)
                            each_sentence_score.append(sentence_sentiment_score)

                        # pprint.pprint(each_sentence_score)

                        embd_url_payload['title'] = response.title

                        embd_url_payload['avg_analysis'] = {
                            "compound": sum(item['compound'] for item in each_sentence_score) / float(
                                len(each_sentence_score)),
                            "positive_score": sum(item['positive_score'] for item in each_sentence_score) / float(
                                len(each_sentence_score)),
                            "negative_score": sum(item['negative_score'] for item in each_sentence_score) / float(
                                len(each_sentence_score)),
                            "sentences_analyzed": len(sentences),

                        }

                        print("         ::SENTENCE SCORES :::::::{} {} {}".format(
                            float(len(each_sentence_score)),
                            [ item['compound'] for item in each_sentence_score ],
                            embd_url_payload['avg_analysis']

                        ))

                        # now lets a look for news stories based on our tweet

                        save_this = Sentimentalite()
                        save_this.sentiment_score = sentiment_score
                        save_this.tweet_id = tweet_id
                        save_this.absolute_sentiment = absolute_sentiment
                        save_this.key_words = key_words
                        save_this.tweet_payload = tweet_payload
                        save_this.geo = geo
                        save_this.embedded_url_payload = embd_url_payload
                        save_this.sentences_score = each_sentence_score

                        save_this.save()
                    else:
                        if not Tweetstream.objects.filter(tweet_id=tweet_id):
                            print("             ::::::::::::::::::::: NOT ENOUGH SENTENCES AND DONT HAVE :: SAVING TWEET")
                            save_this = Tweetstream()
                            save_this.sentiment_score = sentiment_score
                            save_this.tweet_id = tweet_id
                            save_this.absolute_sentiment = absolute_sentiment
                            save_this.key_words = key_words
                            save_this.tweet_payload = tweet_payload
                            save_this.geo = geo
                        else:
                            print("             :::::::::::::::::::::::::NO EMBEDDED URL ::: ALREADY HAVE TWEET :: NOOP")
                else:
                    if not Tweetstream.objects.filter(tweet_id=tweet_id):
                        print("             :::::::::::::::::: ALREADY HAVE URL BUT SAVING TWEET".format())
                        save_this = Tweetstream()
                        save_this.sentiment_score = sentiment_score
                        save_this.tweet_id = tweet_id
                        save_this.absolute_sentiment = absolute_sentiment
                        save_this.key_words = key_words
                        save_this.tweet_payload = tweet_payload
                        save_this.geo = geo
                    else:
                        print("             ::::::::::::::::::::::::::::::::::::::::::: NO-OP ALREADY HAVE TWEET")
            else:
                print("             :::::::::::::::::::::::::::::::::::::::::::NO EMBEDDED URL")
                if not Tweetstream.objects.filter(tweet_id=tweet_id):
                    print("             ::::::::::::::::::::::::::::::::::::::::NO EMBEDDED URL ::: DONT HAVE :: SAVE TWEET")
                    save_this = Tweetstream()
                    save_this.sentiment_score = sentiment_score
                    save_this.tweet_id = tweet_id
                    save_this.absolute_sentiment = absolute_sentiment
                    save_this.key_words = key_words
                    save_this.tweet_payload = tweet_payload
                    save_this.geo = geo

                    save_this.save()
                else:
                    print("             :::::::::::::::::::::::::::::::::NO EMBEDDED URL ::: ALREADY HAVE TWEET :: NOOP")

        __wobble__ = random.uniform(.5, .77) ** random.uniform(.77, .99)

        print("::: POLITIKWEET STATUS END ::::::::::::::::::::::::::::::::::::;;;;;;;;;;;;;;;;;;;;; {}  {}".format(
            datetime.datetime.utcnow().isoformat(),
            "SLEEPING :: {}/s \n".format( __wobble__)),
        )

        time.sleep(__wobble__)


def main():
    myStreamListener = MyStreamListener()
    myStream = tweepy.Stream(auth=API.auth, listener=myStreamListener)
    stop = False
    trending = []
    #get trending
    try:
        api = tweepy.API(API.auth)
        trending = list(set(
            [urldecode(item.get('query',None)) for item in api.trends_place(1).pop().get('trends',[]) if item.get('tweet_volume',0)]
        ))
    except BaseException as e:
        print(e)
        pass



    while True:
        try:
            myStream.filter(
                track=[
                    "president",
                    "congress",
                    "senate",
                    "house of representatives",
                    "white house",
                    "supreme court",
                    "politics",
                    "political",
                    "demagogue",
                    "Department of Agriculture",
                    "Department of Commerce",
                    "Department of Defense",
                    "Department of Education",
                    "Department of Health and Human Services",
                    "Department of Homeland Security",
                    "Department of Housing and Urban Development",
                    "Department of Energy"] + trending,
                languages=['en'])
        except (KeyboardInterrupt):
            stop = True
            print('Stopping Twitter Streaming Client')
        except Exception as e:
            import sys
            import traceback

            backtrace_text = ''.join(traceback.format_exception(*sys.exc_info()))
            print(backtrace_text)

            _zz_ = random.randint(7,11)
            time.sleep(_zz_)
            print("::::::::::::::::::::::::::::: MAIN LOOP SLEEPING ::: {}/s".format(_zz_))
            continue

        if stop:
            break

        return None


if __name__ == '__main__':
    main()
