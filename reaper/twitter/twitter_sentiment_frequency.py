import mongoengine
import datetime
from bson.code import Code
from dateutil.parser import parse
from reaper.twitter.models import Tweetstream, SentimentAccumulator

# mongo engine
MONGO_HOST = 'ds041861.mlab.com:41861'
MONGO_PORT = 41861
MONGO_DATABASE = "criticalitude"
MONGO_USERNAME = "jack"
MONGO_PASSWORD = "44326373333ere56dfaza"

# connect to mongoengine


mongoengine.connect(
     host="""mongodb://politikweet-www:44326373333ere56dfaza@politikweet-shard-00-00.geybn.mongodb.net:27017,politikweet-shard-00-01.geybn.mongodb.net:27017,politikweet-shard-00-02.geybn.mongodb.net:27017/criticalitude?ssl=true&replicaSet=atlas-qv1y8x-shard-0&authSource=admin&retryWrites=true&w=majority"""

)


def map_reduce():
    """
    calculate the frequency and sentiment
    """
    print('Executing MapReduce {}'.format(datetime.datetime.utcnow().isoformat()))
    _map = Code("function() {"
                "var d=this.created_date;"
                "var key = (this.created_date.getMonth()+1) + '-' + this.created_date.getDate() + '-' + this.created_date.getFullYear();"
                "emit(key, this.sentiment_score.compound);"
                "}")
    _reduce = Code("function(key, values) {"
                   "  return Array.sum(values)/values.length;"
                   "}")

    # run a map/reduce operation spanning all posts
    results = Tweetstream.objects().all().map_reduce(_map, _reduce, "twitter_sentiment_reduction")
    for idx, item in enumerate(list(results)):
        print(idx,item)
        try:
            the_date = parse(item.key).replace(hour=0, minute=0, second=0, microsecond=0)
        except Exception as e:
            the_date = datetime.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)

        current_document = SentimentAccumulator.objects().filter(date=the_date).first()

        if current_document:
            print('updating {} from:{} to: {}'.format(the_date, current_document.value, item.value))
            value = (current_document.value * 1.0 + item.value * 1.0) / 2.0
        else:
            print('creating {}'.format(the_date, item.value))
            value = item.value

        test = SentimentAccumulator.objects(date=the_date).upsert_one(
            date=the_date,
            value=value
        )
        print(test.__dict__)


map_reduce()
