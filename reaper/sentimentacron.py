from nltk.sentiment.vader import SentimentIntensityAnalyzer


class Sentimentalyzer(object):
    def __init__(self):
        self.sid = SentimentIntensityAnalyzer()
        return

    def sentiment_analysis(self, sentences):
        """analyses tweets for sentiment. sentences can be a single sentence or a iterable"""
        pol_score = self.sid.polarity_scores(sentences)

        sentiment_score_range = {
            "negative_score": pol_score['neg'],
            "positive_score": pol_score['pos'],
            "compound": pol_score['compound']
        }

        return sentiment_score_range

    def iround(self, x):
        """iround(number) -> integer
        Round a number to the nearest integer."""
        return int(round(x) - .5) + (x > 0)

    def absolute_sentiment(self, data):

        absolute_sentiment = data.get('compound')

        if absolute_sentiment > 0:
            absolute_sentiment = "POSITIVE"
        elif absolute_sentiment < 0:
            absolute_sentiment = "NEGATIVE"
        else:
            absolute_sentiment = "NEUTRAL"

        return_payload = {
            "absolute_sentiment": absolute_sentiment,
        }

        return return_payload
